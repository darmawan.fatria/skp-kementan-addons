# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv


# to do change the value
class project_project(osv.Model):
    _inherit = 'project.project'

    def set_propose(self, cr, uid, ids, context=None):
        print "set propose inherit....."
        try:
            for target in self.browse(cr, uid, ids, context=context):
                if target.user_id and target.employee_id:
                    emp = target.employee_id
                    print "IS ATASAN........",emp.is_atasan
                    if emp.is_atasan:
                        p_user_id = target.user_id.id
                        domain = p_user_id, target.period_month, target.period_year
                        query = '''
                            select count(skp.id) total from project_project skp
                                left join project_skp realisasi on realisasi.id = skp.id
                                left join project_skp atasan on atasan.id = skp.id and ( atasan.state='propose' or atasan.state='rejected_bkd' )
                                left join project_skp atasan_done on atasan_done.id = skp.id and ( atasan_done.state='appeal' or atasan_done.state='evaluated' or atasan_done.state='closed' or atasan_done.state='propose_to_close' )
                                left join project_skp done on done.id = skp.id and done.state='done'
                                left join project_skp review on review.id = skp.id and review.state='propose_to_review'
                                where skp.user_id_atasan = %s
                                 and skp.active
                                 and skp.target_period_month= %s
                                 and skp.target_period_year = %s
                                 and ( skp.state ='realisasi' or skp.state ='rejected_manager' or skp.state ='propose')

                            '''
                        self.env.cr.execute(query, domain)

        except:
            print "do nothing"
        return super(project_project, self).set_propose(cr, uid, uid, context=context)




project_project()
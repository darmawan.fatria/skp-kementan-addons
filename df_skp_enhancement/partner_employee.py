# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime,timedelta
import time

class partner_employee(osv.Model):
    _inherit = 'res.partner'

    
    def is_atasan(self, cr, uid,ids, context=None):
        if context is None:
            context = {}
        res = {}

        for emp in self.browse(cr,uid,ids,context=None):
            res[emp.id] = False
            if emp.employee and emp.user_id:
                user_obj = emp.user_id
                for grp in user_obj.group_id:
                    print grp.name
                    print grp.id
                    if grp.gid == 23 : # atasan
                        res[emp.id] = True



        return res

    _columns = {
        # SKP
        'is_atasan': fields.function(is_atasan, method=True, readonly=True, store=False,
                                     type="boolean", string='Role Atasan'),

    }

    
partner_employee()



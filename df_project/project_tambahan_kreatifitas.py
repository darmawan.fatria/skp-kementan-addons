# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from datetime import datetime, date
from lxml import etree
import time

from openerp import SUPERUSER_ID
from openerp import tools
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp.tools.translate import _

_KATEGORI_KEIKUTSERTAAN_TAMBAHAN_KREATIFITAS_LIST = [('ketua', 'Ketua'),
                           ('sekretaris', 'Sekretaris'),
                           ('bendahara', 'Bendahara'),
                           ('koordinator', 'Koordinator Bidang'),
                           ('anggota', 'Anggota'),
                           ]
class project_tambahan_kreatifitas(osv.Model):
    _name = 'project.tambahan.kreatifitas'
    _description='Realisasi Tugas Tambahan Dan Kreatifitas' 
    
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    def write(self, cr, uid, ids, vals, context=None):
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
                task_id = task_obj.id
                if uid != 1:
                    if task_obj.state in ('draft', 'realisasi', 'rejected_manager', 'cancelled'):
                        if not self.is_user(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('propose', 'rejected_bkd'):
                        if not self.is_manager(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('appeal'):
                        if not self.is_appeal_manager(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('evaluated'):
                        if not self.is_verificator(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('done'):
                        if not self.is_verificator(cr, uid, ids, context) :
                            return False
        super(project_tambahan_kreatifitas, self).write(cr, uid, ids, vals, context=context)           
        return True
    
   
    
    _columns = {
        'name': fields.char('Nama Kegiatan', size=500, readonly=True,),
        'code'     : fields.char('Kode Kegiatan', size=20,),
        'active': fields.boolean('Active', help="If the active field is set to False, it will allow you to hide the project without removing it."),
        'target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')], 'Periode Bulan'
                                                     ),
        'state': fields.selection([('draft', 'Draft'), ('realisasi', 'Realisasi'),
                                        ('propose', 'Atasan'), ('rejected_manager', 'Pengajuan Ditolak Atasan'),
                                        ('appeal', 'Keberatan'), ('evaluated', 'Verifikasi'), ('rejected_bkd', 'Pengajuan Ditolak Verifikatur'),
                                        ('propose_to_close','Pengajuan Closing Target'),('closed','Closed'),
                                        ('done', 'Selesai'), ('cancelled', 'Cancel')], 'Status Pekerjaan'),
        'target_period_year'     : fields.char('Periode Tahun', size=4, required=True),
        
        'user_id': fields.many2one('res.users', 'Pegawai Yang Dinilai',),
        'user_id_atasan': fields.many2one('res.users', 'Pejabat Penilai',),
        'user_id_banding': fields.many2one('res.users', 'Atasan Pejabat Penilai',),
        'user_id_bkd': fields.many2one('res.users', 'Verifikatur',),
        'notes'     : fields.text('Catatan', states={'draft': [('required', False)]}),
        'notes_atasan'     : fields.text('Catatan Atasan', ),
        'notes_atasan_banding'     : fields.text('Catatan Atasan Pejabat Penilai', ),
        'notes_bkd'     : fields.text('Catatan Petugas Verifikasi', ),
        
        'is_suggest': fields.boolean('Tambahkan Koreski Penilaian'),
        'is_appeal': fields.boolean('Tambahkan Koreski Keberatan'),
        'is_control': fields.boolean('Verifikasi Penilaian'),
        
        'realisasi_tugas_tambahan'     : fields.integer('Jumlah Tugas Tambahan'),
        'realisasi_uraian_tugas_tambahan'     : fields.text('Uraian Tugas Tambahan'),
        'realisasi_rl_mentri_tugas_tambahan'     : fields.boolean('Menteri'),
        'realisasi_rl_es1_tugas_tambahan'     : fields.boolean('Eselon 1'),
        'realisasi_rl_es2_tugas_tambahan'     : fields.boolean('Eselon 2'),
        'realisasi_rl_kepala_tugas_tambahan': fields.boolean('Kepala UPT/Atasan Langsung'),
        'realisasi_rl_mentri_keikutsertaan_tugas_tambahan': fields.selection(_KATEGORI_KEIKUTSERTAAN_TAMBAHAN_KREATIFITAS_LIST,
                                               'Keterlibatan Dalam Tim',
                                               ),
        'realisasi_rl_es1_keikutsertaan_tugas_tambahan': fields.selection(
            _KATEGORI_KEIKUTSERTAAN_TAMBAHAN_KREATIFITAS_LIST,
            'Keterlibatan Dalam Tim',
            ),
        'realisasi_rl_es2_keikutsertaan_tugas_tambahan': fields.selection(
            _KATEGORI_KEIKUTSERTAAN_TAMBAHAN_KREATIFITAS_LIST,
            'Keterlibatan Dalam Tim',
        ),
        'realisasi_rl_kepala_keikutsertaan_tugas_tambahan': fields.selection(
            _KATEGORI_KEIKUTSERTAAN_TAMBAHAN_KREATIFITAS_LIST,
            'Keterlibatan Dalam Tim',
        ),
        'realisasi_attach_tugas_tambahan'              : fields.binary('SK/SP/ST'),
        'realisasi_nilai_kreatifitas'     : fields.integer('Jumlah Kreatifitas'),
        'realisasi_uraian_kreatifitas'     : fields.text('Uraian Kreatifitas'),
        'realisasi_attach_kreatifitas'              : fields.binary('Bukti Pengakuan'),
        'realisasi_tupoksi_kreatifitas': fields.boolean('Tupoksi'),
        'realisasi_rl_presiden_kreatifitas'     : fields.boolean('Menteri'),
        'realisasi_rl_gubernur_kreatifitas'     : fields.boolean('Eselon 1'),
        'realisasi_rl_opd_kreatifitas'     : fields.boolean('Eselon 2'),
        'realisasi_rl_presiden_keikutsertaan_kreatifitas': fields.selection(_KATEGORI_KEIKUTSERTAAN_TAMBAHAN_KREATIFITAS_LIST,
                                                                      'Keterlibatan Dalam Tim',
                                                                      ),
        'realisasi_rl_gubernur_keikutsertaan_kreatifitas': fields.selection(
            _KATEGORI_KEIKUTSERTAAN_TAMBAHAN_KREATIFITAS_LIST,
            'Keterlibatan Dalam Tim',
            ),
        'realisasi_rl_opd_keikutsertaan_kreatifitas': fields.selection(
            _KATEGORI_KEIKUTSERTAAN_TAMBAHAN_KREATIFITAS_LIST,
            'Keterlibatan Dalam Tim',
        ),
        'suggest_tugas_tambahan'     : fields.integer('Jumlah Tugas Tambahan'),
        'suggest_uraian_tugas_tambahan'     : fields.text('Uraian Tugas Tambahan'),
        'suggest_rl_mentri_tugas_tambahan'     : fields.boolean('Menteri'),
        'suggest_rl_es1_tugas_tambahan'     : fields.boolean('Eselon 1'),
        'suggest_rl_es2_tugas_tambahan'     : fields.boolean('Eselon 2'),
        'suggest_rl_kepala_tugas_tambahan': fields.boolean('Kepala UPT/Atasan Langsung'),
        'suggest_attach_tugas_tambahan'              : fields.binary('SK/SP/ST'),
        'suggest_nilai_kreatifitas'     : fields.integer('Jumlah Kreatifitas'),
        'suggest_uraian_kreatifitas'     : fields.text('Uraian Kreatifitas'),
        'suggest_attach_kreatifitas'              : fields.binary('Bukti Pengakuan'),
        'suggest_tupoksi_kreatifitas': fields.boolean('Tupoksi'),
        'suggest_rl_presiden_kreatifitas'     : fields.boolean('Menteri'),
        'suggest_rl_gubernur_kreatifitas'     : fields.boolean('Eselon 1'),
        'suggest_rl_opd_kreatifitas'     : fields.boolean('Eselon 2'),
        
        'appeal_tugas_tambahan'     : fields.integer('Jumlah Tugas Tambahan'),
        'appeal_uraian_tugas_tambahan'     : fields.text('Uraian Tugas Tambahan'),
        'appeal_rl_mentri_tugas_tambahan'     : fields.boolean('Menteri'),
        'appeal_rl_es1_tugas_tambahan'     : fields.boolean('Eselon 1'),
        'appeal_rl_es2_tugas_tambahan'     : fields.boolean('Eselon 2'),
        'appeal_rl_kepala_tugas_tambahan': fields.boolean('Kepala UPT/Atasan Langsung'),
        'appeal_attach_tugas_tambahan'              : fields.binary('SK/SP/ST'),
        'appeal_nilai_kreatifitas'     : fields.integer('Jumlah Kreatifitas'),
        'appeal_uraian_kreatifitas'     : fields.text('Uraian Kreatifitas'),
        'appeal_attach_kreatifitas'              : fields.binary('Bukti Pengakuan'),
        'appeal_tupoksi_kreatifitas': fields.boolean('Tupoksi'),
        'appeal_rl_presiden_kreatifitas'     : fields.boolean('Menteri'),
        'appeal_rl_gubernur_kreatifitas'     : fields.boolean('Eselon 1'),
        'appeal_rl_opd_kreatifitas'     : fields.boolean('Kepala Instansi'),
        
        'control_count': fields.integer('Jumlah Pengajuan Verifikasi', readonly=True),
        'nilai_akhir': fields.float('Nilai', readonly=True),
        'nilai_sementara': fields.float('Nilai Sementara', readonly=True),
        'jumlah_perhitungan': fields.float('Jumlah Perhitungan', readonly=True),
        'nilai_tambahan': fields.float('Nilai Tambahan', readonly=True),
        'nilai_kreatifitas': fields.float('Nilai Kreatifitas', readonly=True),
        'indeks_nilai': fields.char('Indeks', size=20, readonly=True),
        
        
        'company_id': fields.many2one('res.company', 'Instansi'),
        'currency_id': fields.many2one('res.currency', 'Currency'),
        'employee_id': fields.related('user_id', 'partner_id',   type="many2one", relation='res.partner', string='Tipe Jabatan', store=True),
        'employee_job_type': fields.selection([('struktural', 'Jabatan Struktural'), ('jft', 'Jabatan Fungsional Tertentu'), ('jfu', 'Jabatan Fungsional Umum')], 'Tipe Jabatan'),
        'job_id': fields.related('employee_id', 'job_id',   type="many2one", relation='partner.employee.job', string='Jabatan', store=False),
        'department_id': fields.related('employee_id', 'department_id', type="many2one",
                                        relation='partner.employee.department',
                                        string='Unor ess.II/UPT', store=True),
        'is_kepala_opd': fields.related('employee_id', 'is_kepala_opd', type='boolean', string='Tugas Untuk Kepala Instansi', store=True),
        'use_target_for_calculation': fields.boolean('Perhitungan Menggunakan Target',help="Pengakuan perhitungan berdasar target, Bukan Realisasi"),
    }
    _defaults = {
        'user_id': lambda self, cr, uid, ctx: uid,
        'state':'draft',
        'name':'',
        'target_period_year':lambda *args: time.strftime('%Y'),
        'use_target_for_calculation':False,
        
    }
    _order = "target_period_year,target_period_month"
    
    #VALIDATOR
    def get_auth_id(self, cr, uid, ids, type, context=None):
        if not isinstance(ids, list): ids = [ids]
        for task in self.browse(cr, uid, ids, context=context):
            
            if type == 'user_id' :
                if task.user_id :
                    if task.user_id.id != uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_atasan' :
                if task.user_id_atasan :
                    if task.user_id_atasan.id != uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_banding' :
                if task.user_id_banding :
                    if task.user_id_banding.id != uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_bkd' :
                if task.user_id_bkd :
                    if task.user_id_bkd.id != uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
                
            else : 
               return False;
            
        return True;
    def is_user(self,cr,uid,ids,context=None):
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        return self.get_auth_id(cr, uid, [task_id],'user_id', context=context) 
    def is_manager(self,cr,uid,ids,context=None):
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        return self.get_auth_id(cr, uid, [task_id],'user_id_atasan', context=context) 
    def is_appeal_manager(self,cr,uid,ids,context=None):
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        return self.get_auth_id(cr, uid, [task_id],'user_id_banding', context=context) 
    def is_verificator(self,cr,uid,ids,context=None):
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        return self.get_auth_id(cr, uid, [task_id],'user_id_bkd', context=context)
    def is_user_or_is_verificator(self,cr,uid,ids,context=None):
        task_id = len(ids) and ids[0] or False
        if not task_id: return False
        accepted = self.get_auth_id(cr, uid, [task_id],'user_id', context=context) or self.get_auth_id(cr, uid, [task_id],'user_id_bkd', context=context)
        return accepted
    
    
    # WORKFLOW    
    def set_draft(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'draft'}, context=context) 
    def set_realisasi(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'realisasi'}, context=context)
    def set_cancelled(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'cancelled','active':False}, context=context) 
    def set_closed(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'closed'}, context=context)
    def set_propose(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'propose'}, context=context)
    def set_appeal(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'appeal'}, context=context)
    def set_evaluated(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'evaluated'}, context=context)
    def set_done(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'done'}, context=context)
    def set_rejected_bkd(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'rejected_bkd'}, context=context)
    def action_realisasi(self, cr, uid, ids, context=None):
        """ Khusus untuk kegiatan perilaku dan  tambahan & kreatifitas.
            Kondisi pegawai siap mengisikan realisasi
        """
        if self.is_user(cr, uid, ids, context) :
            return self.set_realisasi(cr, uid, ids, context)
            
        
    def action_propose(self, cr, uid, ids, context=None):
        """ Selesai mengerjakan input realisasi, 
        """
        if self.is_user(cr, uid, ids, context) :
            self.fill_target_automatically_with_task(cr, uid, ids, context);
            return self.set_propose(cr, uid, ids, context=context)
    def action_cancel_realisasi(self, cr, uid, ids, context=None):
        """ Batalkan Transaksi 
        """
        if self.is_user(cr, uid, ids, context) :
            return self.set_cancelled(cr, uid, ids, context=context)
    def action_propose_rejected_tambahan_kreatifitas_popup(self, cr, uid, ids, context=None):
        if not ids: return []
        if self.is_manager(cr, uid, ids, context):
                task_obj = self.browse(cr, uid, ids[0], context=context)
                
                dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_project', 'action_tambahan_propose_rejected_popup_form_view')
                return {
                'name':_("Pengajuan Tugas Tambahan Dan Kreatifitas DiTolak"),
                'view_mode': 'form',
                'view_id': view_id,
                'view_type': 'form',
                'res_model': 'project.tambahan.kreatifitas.propose.rejected',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': {
                               'default_tugas_tambahan'              : task_obj.realisasi_tugas_tambahan,
                                'default_uraian_tugas_tambahan'       : task_obj.realisasi_uraian_tugas_tambahan,
                                'default_rl_kepala_tugas_tambahan': task_obj.realisasi_rl_kepala_tugas_tambahan,
                                'default_rl_es2_tugas_tambahan'       : task_obj.realisasi_rl_es2_tugas_tambahan,
                                'default_rl_es1_tugas_tambahan'  : task_obj.realisasi_rl_es1_tugas_tambahan,
                                'default_rl_mentri_tugas_tambahan'  : task_obj.realisasi_rl_mentri_tugas_tambahan,
                                'default_nilai_kreatifitas'           : task_obj.realisasi_nilai_kreatifitas,
                                'default_uraian_kreatifitas'          : task_obj.realisasi_uraian_kreatifitas,
                                'default_tupoksi_kreatifitas'         : task_obj.realisasi_tupoksi_kreatifitas,
                                'default_rl_opd_kreatifitas'          : task_obj.realisasi_rl_opd_kreatifitas,
                                'default_rl_gubernur_kreatifitas'     : task_obj.realisasi_rl_gubernur_kreatifitas,
                                'default_rl_presiden_kreatifitas'     : task_obj.realisasi_rl_presiden_kreatifitas,
                                    
                             'default_is_suggest': True,
                             'default_task_id':task_obj.id
    
                }
            }
        return False
    
    def action_appeal(self, cr, uid, ids, context=None):
        """ Penolakan di Keberatan ke Atasan
        """
        if self.is_user(cr, uid, ids, context) :
            self.fill_task_appeal_automatically_with_suggest(cr, uid, ids, context);
            return self.set_appeal(cr, uid, ids, context=context)
    def action_dont_appeal(self, cr, uid, ids, context=None):
        """ Penolakan Diterima, Langsung ajukan Ke Verifikatur,
        """
        if self.is_user(cr, uid, ids, context) :
            self.fill_task_automatically_with_suggest(cr, uid, ids, context);
            return self.set_evaluated(cr, uid, ids, context=context)
    def action_appeal_approve(self, cr, uid, ids, context=None):
        """ Keberatan DIterima, Langsung ajukan Ke Verifikatur,
        """
        if self.is_appeal_manager(cr, uid, ids, context) : 
            return self.set_evaluated(cr, uid, ids, context=context)

    
    def action_appeal_rejected_tambahan_kreatifitas_popup(self, cr, uid, ids, context=None):
        
        if not ids: return []
        
        

        if self.is_appeal_manager(cr, uid, ids, context):
                task_obj = self.browse(cr, uid, ids[0], context=context)
                dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_project', 'action_tambahan_appeal_rejected_popup_form_view')
                return {
                'name':_("Pengajuan Keberatan Tugas Tambahan Dan Kreatifitas Ditolak"),
                'view_mode': 'form',
                'view_id': view_id,
                'view_type': 'form',
                'res_model': 'project.tambahan.kreatifitas.appeal.rejected',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': {
                               'default_tugas_tambahan'              : task_obj.suggest_tugas_tambahan,
                                'default_uraian_tugas_tambahan'       : task_obj.suggest_uraian_tugas_tambahan,
                                'default_rl_kepala_tugas_tambahan': task_obj.suggest_rl_kepala_tugas_tambahan,
                                'default_rl_es2_tugas_tambahan'       : task_obj.suggest_rl_es2_tugas_tambahan,
                                'default_rl_es1_tugas_tambahan'  : task_obj.suggest_rl_es1_tugas_tambahan,
                                'default_rl_mentri_tugas_tambahan'  : task_obj.suggest_rl_mentri_tugas_tambahan,
                                'default_nilai_kreatifitas'           : task_obj.suggest_nilai_kreatifitas,
                                'default_uraian_kreatifitas'          : task_obj.suggest_uraian_kreatifitas,
                                'default_tupoksi_kreatifitas'         : task_obj.suggest_tupoksi_kreatifitas,
                                'default_rl_opd_kreatifitas'          : task_obj.suggest_rl_opd_kreatifitas,
                                'default_rl_gubernur_kreatifitas'     : task_obj.suggest_rl_gubernur_kreatifitas,
                                'default_rl_presiden_kreatifitas'     : task_obj.suggest_rl_presiden_kreatifitas,
                                    
                           
                             'default_is_appeal': True,
                             'default_task_id':task_obj.id
    
                }
            }
        return False
    def action_evaluated(self, cr, uid, ids, context=None):
        """ Ajukan Ke Verifikatur
        """
        if self.is_manager(cr, uid, ids, context) :
            return self.set_evaluated(cr, uid, ids, context=context)
    def action_done(self, cr, uid, ids, context=None):
        """ Selesai Perhitungan
        """
        for task_id in ids:
            if self.is_verificator(cr, uid, [task_id], context) :
                self.do_task_poin_calculation(cr, uid, [task_id], context=context)
                self.set_done(cr, uid, [task_id], context=context)
        return True;
    def action_work_rejected(self, cr, uid, ids, context=None):
        """ Verifikasi Ditolak
        """
        
        if not ids: return []
        
        if self.is_verificator(cr, uid, ids, context) :
            dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_project', 'action_verificate_tambahan_kreatifitas_rejected_popup_form_view')
            task_obj = self.browse(cr, uid, ids[0], context=context)
            if task_obj.control_count >= 2 :
                raise osv.except_osv(_('Invalid Action, Limit Action'),
                                         _('Hasil Verifikasi Tidak Dapat Ditolak, Karena Sudah Dilakukan Pengajuan Dan Verifikasi Sebanyak 2 Kali.'))
            return {
                'name':_("Verifikasi Ditolak"),
                'view_mode': 'form',
                'view_id': view_id,
                'view_type': 'form',
                'res_model': 'project.tambahan.kreatifitas.verificate.rejected',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': {
                    'default_control_count':task_obj.control_count + 1,
                    'default_is_control': True,
                    'default_task_id':task_obj.id
    
                }
            }
        return False
    def action_done_use_target(self, cr, uid, ids, context=None):
        """ Selesai Perhitungan
        """
        return True;
    def do_recalculate_poin(self, cr, uid, ids, context=None):
         if self.is_verificator(cr, uid, ids, context) :
            
            update_poin = {
                                        'nilai_akhir': 0,
                                        'indeks_nilai': False,
                                        'jumlah_perhitungan':0,
                                        'nilai_tambahan': 0,
                                        'nilai_kreatifitas': 0,
                                        'use_target_for_calculation': False,
                                         }
            self.write(cr, uid, ids, update_poin)
            return self.set_evaluated(cr, uid, ids, context=context)
                 
         return True;
    def action_verificate_revision(self, cr, uid, ids, context=None):
        """ Selesai Perhitungan
        """
        if self.is_manager(cr, uid, ids, context) :
            self.fill_task_automatically_with_suggest(cr, uid, ids, context);
            return self.set_evaluated(cr, uid, ids, context=context)
    #Calculation
    def do_task_poin_calculation(self, cr, uid, ids, context=None):
        """ BKD->Done (Keuangan) """
        for task_id in ids:
            update_poin = self.prepare_task_poin_calculation(cr, uid, [task_id], context=context)
            if update_poin:
                self.write(cr, uid, [task_id], update_poin,context)
        return True
    def do_task_poin_calculation_temporary(self, cr, uid, ids, context=None):
        
        for task_id in ids :
            update_poin = self.prepare_task_poin_calculation(cr, uid, [task_id], context=context)
            if update_poin:
                nilai_sementara = update_poin.get('nilai_akhir',0)
                self.write(cr, uid, [task_id], {
                                    'nilai_sementara': nilai_sementara}
                           ,context)
        return True
    def prepare_task_poin_calculation(self, cr, uid, ids, context=None):
        """ BKD->Done (Keuangan) """
        #print "Poin Calculation"
        
        lookup_nilai_pool = self.pool.get('acuan.penilaian')
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
            vals = {}
            
            # close task...
            nilai_tambahan=0
            nilai_kreatifitas=0
            nilai_akhir = 0
            jumlah_perhitungan = 0
            indeks_nilai = 'a'
            
            employee = task_obj.user_id and task_obj.user_id.partner_id
            
            if not employee :
                 raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena User Login Belum Dikaitkan Dengan Data Pegawai.'))
            else :
                job_type = employee.job_type
                if not job_type :
                     raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Jenis Jabatan Pegawai Belum Diisi, Harap Dilengkapi Terlebih Dahulu Di Data Pegawai, Atau Data Jabatan.'))
            if task_obj:
                    a = b = 0
                    if task_obj.realisasi_tugas_tambahan >= 1 :
                        try :
                            if task_obj.realisasi_rl_mentri_tugas_tambahan:
                                lookup_nilai_id = lookup_nilai_pool.search(cr, uid,
                                                                           [('kategori_tambahan', '=', 'mentri'),
                                                                            ('type', '=', 'tugas_tambahan')], context=None)
                                lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                                jenis_keikutsertaan  = task_obj.realisasi_rl_mentri_keikutsertaan_tugas_tambahan
                                nilai_tambahan = self.get_nilai_proporsi_tambahan_kreatifitas_keikutsertaan(lookup_nilai.nilai_tunggal,jenis_keikutsertaan)
                            if task_obj.realisasi_rl_es1_tugas_tambahan:
                                lookup_nilai_id = lookup_nilai_pool.search(cr, uid,
                                                                           [('kategori_tambahan', '=', 'es1'),
                                                                            ('type', '=', 'tugas_tambahan')], context=None)
                                lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                                jenis_keikutsertaan = task_obj.realisasi_rl_es1_keikutsertaan_tugas_tambahan
                                nilai_tambahan = nilai_tambahan + self.get_nilai_proporsi_tambahan_kreatifitas_keikutsertaan(
                                    lookup_nilai.nilai_tunggal, jenis_keikutsertaan)

                            if task_obj.realisasi_rl_es2_tugas_tambahan:
                                lookup_nilai_id = lookup_nilai_pool.search(cr, uid,
                                                                           [('kategori_tambahan', '=', 'es2'),
                                                                            ('type', '=', 'tugas_tambahan')], context=None)
                                lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                                jenis_keikutsertaan = task_obj.realisasi_rl_es2_keikutsertaan_tugas_tambahan
                                nilai_tambahan = nilai_tambahan + self.get_nilai_proporsi_tambahan_kreatifitas_keikutsertaan(
                                    lookup_nilai.nilai_tunggal, jenis_keikutsertaan)
                            if task_obj.realisasi_rl_kepala_tugas_tambahan:
                                lookup_nilai_id = lookup_nilai_pool.search(cr, uid,
                                                                           [('kategori_tambahan', '=', 'kepala_upt'),
                                                                            ('type', '=', 'tugas_tambahan')], context=None)
                                lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                                jenis_keikutsertaan = task_obj.realisasi_rl_es2_keikutsertaan_tugas_tambahan
                                nilai_tambahan = nilai_tambahan + self.get_nilai_proporsi_tambahan_kreatifitas_keikutsertaan(
                                    lookup_nilai.nilai_tunggal, jenis_keikutsertaan)

                            #print "nilai tambahan : ", nilai_tambahan
                            maks_tambahan = 38
                            if nilai_tambahan > maks_tambahan :
                                nilai_tambahan = maks_tambahan;
                            #print "nilai tambahan : ", nilai_tambahan
                            a = nilai_tambahan;
                        except:
                            raise osv.except_osv(_('Invalid Action, Konfigurasi Lookup Nilai Tidak Lengkap'),
                                _('Konfigurasi Penilaian Tugas Tambahan Belum Di Atur, Silahkan Hubungi Admin Aplikasi.'))
                    if task_obj.realisasi_nilai_kreatifitas >= 1 :
                        try :
                            if task_obj.realisasi_rl_presiden_kreatifitas:
                                lookup_nilai_id = lookup_nilai_pool.search(cr, uid,
                                                                           [('kategori_kreatifitas', '=', 'presiden'),
                                                                            ('type', '=', 'kreatifitas')], context=None)
                                lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                                jenis_keikutsertaan = task_obj.realisasi_rl_presiden_keikutsertaan_kreatifitas
                                nilai_kreatifitas = self.get_nilai_proporsi_tambahan_kreatifitas_keikutsertaan(
                                    lookup_nilai.nilai_tunggal, jenis_keikutsertaan)
                            elif task_obj.realisasi_rl_gubernur_kreatifitas:
                                lookup_nilai_id = lookup_nilai_pool.search(cr, uid,
                                                                           [('kategori_kreatifitas', '=', 'mentri'),
                                                                            ('type', '=', 'kreatifitas')], context=None)
                                lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                                jenis_keikutsertaan = task_obj.realisasi_rl_gubernur_keikutsertaan_kreatifitas
                                nilai_kreatifitas = nilai_kreatifitas + self.get_nilai_proporsi_tambahan_kreatifitas_keikutsertaan(
                                    lookup_nilai.nilai_tunggal, jenis_keikutsertaan)
                            elif task_obj.realisasi_rl_opd_kreatifitas :
                                lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_kreatifitas', '=', 'es1'), ('type', '=', 'kreatifitas')], context=None)
                                lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                                jenis_keikutsertaan = task_obj.realisasi_rl_opd_keikutsertaan_kreatifitas
                                nilai_kreatifitas = nilai_kreatifitas + self.get_nilai_proporsi_tambahan_kreatifitas_keikutsertaan(
                                    lookup_nilai.nilai_tunggal, jenis_keikutsertaan)

                            maks_kreatifitas = 12
                            if nilai_kreatifitas > maks_kreatifitas:
                                nilai_kreatifitas = maks_kreatifitas;
                            b = nilai_kreatifitas

                        except :
                            raise osv.except_osv(_('Invalid Action, Konfigurasi Lookup Nilai Tidak Lengkap'),
                                _('Konfigurasi Penilaian Perilaku Belum Di Atur, Silahkan Hubungi Admin Aplikasi.'))
                    jumlah_perhitungan = (a + b)
                    nilai_akhir = (a + b)
                
                # end if          
            update_poin = {
                                'nilai_akhir': nilai_akhir,
                                'indeks_nilai': '-',
                                'jumlah_perhitungan':jumlah_perhitungan,
                                'nilai_tambahan': nilai_tambahan,
                                'nilai_kreatifitas':nilai_kreatifitas,
                                 }
            return update_poin;
        return False

    def get_nilai_proporsi_tambahan_kreatifitas_keikutsertaan(self, nilai,jenis_keikutsertaan):
        persentasi = 0.75  # 75%
        if jenis_keikutsertaan == 'ketua':
            persentasi = 1.00
        elif jenis_keikutsertaan == 'sekretaris':
            persentasi = 0.90  # 75%
        elif jenis_keikutsertaan == 'bendahara':
            persentasi = 0.90  # 75%
        elif jenis_keikutsertaan == 'koordinator':
            persentasi = 0.90  # 75%
        elif jenis_keikutsertaan == 'anggota':
            persentasi = 0.75  # 75%
        hasil = nilai * persentasi
        return hasil

    def get_value_realisasi_or_target(self, target,realisasi, use_target_for_calculation):
        if use_target_for_calculation : 
           return target
        return realisasi
    #Automation
    
    def fill_target_automatically_with_task(self, cr, uid, ids, context=None):
        """ Default Target di isi task"""
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
                vals = {}
                
                vals.update({
                                    'suggest_tugas_tambahan'     : task_obj.realisasi_tugas_tambahan,
                                    'suggest_uraian_tugas_tambahan'     : task_obj.realisasi_uraian_tugas_tambahan,
                                    'suggest_rl_kepala_tugas_tambahan': task_obj.realisasi_rl_kepala_tugas_tambahan,
                                    'suggest_rl_es2_tugas_tambahan'     : task_obj.realisasi_rl_es2_tugas_tambahan,
                                    'suggest_rl_es1_tugas_tambahan'     : task_obj.realisasi_rl_es1_tugas_tambahan,
                                    'suggest_rl_mentri_tugas_tambahan'     : task_obj.realisasi_rl_mentri_tugas_tambahan,
                                    'suggest_nilai_kreatifitas'     : task_obj.realisasi_nilai_kreatifitas,
                                    'suggest_uraian_kreatifitas'     : task_obj.realisasi_uraian_kreatifitas,
                                    'suggest_tupoksi_kreatifitas'     : task_obj.realisasi_tupoksi_kreatifitas,
                                    'suggest_rl_opd_kreatifitas'     : task_obj.realisasi_rl_opd_kreatifitas,
                                    'suggest_rl_gubernur_kreatifitas'     : task_obj.realisasi_rl_gubernur_kreatifitas,
                                    'suggest_rl_presiden_kreatifitas'     : task_obj.realisasi_rl_presiden_kreatifitas,
                                })
                
                # end if               
                self.write(cr, uid, [task_obj.id], vals, context)
        # end for
        return True
    def fill_task_appeal_automatically_with_suggest(self, cr, uid, ids, context=None):
        """ Jika selesai mebuat target. Maka secara default realisasi akan otomatis terisi dengan nilai target tersebit"""
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
            vals = {}
            
            if task_obj.is_suggest:
                vals.update({
                                   
                                    'appeal_tugas_tambahan'     : task_obj.suggest_tugas_tambahan,
                                    'appeal_uraian_tugas_tambahan'     : task_obj.suggest_uraian_tugas_tambahan,
                                    'appeal_rl_kepala_tugas_tambahan': task_obj.suggest_rl_kepala_tugas_tambahan,
                                    'appeal_rl_es2_tugas_tambahan'     : task_obj.suggest_rl_es2_tugas_tambahan,
                                    'appeal_rl_es1_tugas_tambahan'     : task_obj.suggest_rl_es1_tugas_tambahan,
                                    'appeal_rl_mentri_tugas_tambahan'     : task_obj.suggest_rl_mentri_tugas_tambahan,
                                    'appeal_nilai_kreatifitas'     : task_obj.suggest_nilai_kreatifitas,
                                    'appeal_uraian_kreatifitas'     : task_obj.suggest_uraian_kreatifitas,
                                    'appeal_tupoksi_kreatifitas'     : task_obj.suggest_tupoksi_kreatifitas,
                                    'appeal_rl_opd_kreatifitas'     : task_obj.suggest_rl_opd_kreatifitas,
                                    'appeal_rl_gubernur_kreatifitas'     : task_obj.suggest_rl_gubernur_kreatifitas,
                                    'appeal_rl_presiden_kreatifitas'     : task_obj.suggest_rl_presiden_kreatifitas,
                                })
                # end if               
                self.write(cr, uid, [task_obj.id], vals, context)
        # end for
        return True
    def fill_task_automatically_with_suggest(self, cr, uid, ids, context=None):
        """ Jika selesai mebuat target. Maka secara default realisasi akan otomatis terisi dengan nilai target tersebit"""
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
            vals = {}
            if task_obj.is_suggest:
                vals.update({
                                    
                                    'realisasi_tugas_tambahan'              : task_obj.suggest_tugas_tambahan,
                                    'realisasi_uraian_tugas_tambahan'       : task_obj.suggest_uraian_tugas_tambahan,
                                    'realisasi_rl_kepala_tugas_tambahan': task_obj.suggest_rl_kepala_tugas_tambahan,
                                    'realisasi_rl_es2_tugas_tambahan'       : task_obj.suggest_rl_es2_tugas_tambahan,
                                    'realisasi_rl_es1_tugas_tambahan'  : task_obj.suggest_rl_es1_tugas_tambahan,
                                    'realisasi_rl_mentri_tugas_tambahan'  : task_obj.suggest_rl_mentri_tugas_tambahan,
                                    'realisasi_nilai_kreatifitas'           : task_obj.suggest_nilai_kreatifitas,
                                    'realisasi_uraian_kreatifitas'          : task_obj.suggest_uraian_kreatifitas,
                                    'realisasi_tupoksi_kreatifitas'         : task_obj.suggest_tupoksi_kreatifitas,
                                    'realisasi_rl_opd_kreatifitas'          : task_obj.suggest_rl_opd_kreatifitas,
                                    'realisasi_rl_gubernur_kreatifitas'     : task_obj.suggest_rl_gubernur_kreatifitas,
                                    'realisasi_rl_presiden_kreatifitas'     : task_obj.suggest_rl_presiden_kreatifitas,
                                    
                                })
                # end if               
                self.write(cr, uid, [task_obj.id], vals, context)
        # end for
        return True
    def fill_task_automatically_with_appeal(self, cr, uid, ids, context=None):
        """ Jika selesai mebuat target. Maka secara default realisasi akan otomatis terisi dengan nilai target tersebit"""
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
            vals = {}
            if task_obj.is_appeal:
                vals.update({
                                    'realisasi_tugas_tambahan'     : task_obj.appeal_tugas_tambahan,
                                    'realisasi_uraian_tugas_tambahan'     : task_obj.appeal_uraian_tugas_tambahan,
                                     'realisasi_rl_kepala_tugas_tambahan': task_obj.appeal_rl_kepala_tugas_tambahan,
                                    'realisasi_rl_es2_tugas_tambahan'     : task_obj.appeal_rl_es2_tugas_tambahan,
                                    'realisasi_rl_es1_tugas_tambahan'     : task_obj.appeal_rl_es1_tugas_tambahan,
                                    'realisasi_rl_mentri_tugas_tambahan'     : task_obj.appeal_rl_mentri_tugas_tambahan,
                                    'realisasi_nilai_kreatifitas'     : task_obj.appeal_nilai_kreatifitas,
                                    'realisasi_uraian_kreatifitas'     : task_obj.appeal_uraian_kreatifitas,
                                    'realisasi_tupoksi_kreatifitas'     : task_obj.appeal_tupoksi_kreatifitas,
                                    'realisasi_rl_opd_kreatifitas'     : task_obj.appeal_rl_opd_kreatifitas,
                                    'realisasi_rl_gubernur_kreatifitas'     : task_obj.appeal_rl_gubernur_kreatifitas,
                                    'realisasi_rl_presiden_kreatifitas'     : task_obj.appeal_rl_presiden_kreatifitas,
                                    
                                })
                # end if               
                self.write(cr, uid, [task_obj.id], vals, context)
        # end for
        return True
    
project_tambahan_kreatifitas()
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "Generate Perilaku, Tugas Tambahan Dan Kreatifitas",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "Project/Generate/Non SKP",
    "description": """
    Generate Perilaku Tugas Tambahan Dan Kreatifitas
    """,
    "website" : "http://www.mediasee.net",
    "license" : "GPL-3",
    "depends": [
                "df_project",
                "df_skp_employee"
                ],
    'data': [ 
                   "project_tambahan_kreatifitas_menu_view.xml",
                   "wizard/project_tambahan_kreatifitas_generate_view.xml",
                   "project_perilaku_menu_view.xml",
                   "wizard/project_perilaku_generate_view.xml",
                   "project_generate_admin_menu_view.xml",
                   "wizard/project_perilaku_admin_generate_view.xml",
                   "wizard/project_tambahan_kreatifitas_admin_generate_view.xml",
                   "wizard/skp_employee_admin_generate_view.xml",
                   ],
    'demo_xml': [],
    'installable': True,
    'active': False,

}

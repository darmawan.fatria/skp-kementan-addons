#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2011 OpenERP SA (<http://openerp.com>). All Rights Reserved
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime,timedelta
import time
from openerp.osv import osv
from openerp.report import report_sxw
import locale

_KATEGORI_KEGIATAN_LIST = [('program', 'Program'),
                           ('kegiatan', 'Kegiatan'),
                           ('output', 'Output'),
                           ('suboutput', 'Sub Output'),
                           ]
_JENIS_KEGIATAN_LIST = [('dpa_opd_biro', 'Perjanjian Kerja'),
 ('dipa_apbn', 'Program Kerja'),
 ('sotk', 'Kegiatan Tupoksi'),
 ('lain_lain', 'Upsus'),
 ]
class report_skp_target_monthly_report(report_sxw.rml_parse):
    
    def __init__(self, cr, uid, name, context):
        super(report_skp_target_monthly_report, self).__init__(cr, uid, name, context=context)
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        self.localcontext.update({
            'get_atribut_kepegawaian' : self.get_atribut_kepegawaian,
            'get_skp_monthly_report_raw' : self.get_skp_monthly_report_raw,
            'get_summary_monthly_report_raw' : self.get_summary_monthly_report_raw,
            
            'format_formula_skp':self.format_formula_skp,
            'format_integer' :self.format_integer,
            'format_date_month' :self.format_date_month,
            'format_date_month_year' : self.format_date_month_year,
            'get_atribut_jabatan':self.get_atribut_jabatan,
            'format_date_id':self.format_date_id,
            'get_target_type':self.get_target_type
        })
        
    def get_atribut_kepegawaian(self,filters,context=None):
        period_year=filters['form']['monthly_id'][1]
        user_id = False
        data_pegawai=None
        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]
        user_pool = self.pool.get('res.users')
        if user_id:
            result = user_pool.browse(self.cr, self.uid, user_id)
            if  result and result.partner_id:
                data_pegawai = result.partner_id
        return data_pegawai;
    def get_atribut_jabatan(self,filters,detail_obj,context=None):
        user_id = False

        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]
        user_pool = self.pool.get('res.users')
        if user_id:
            result = user_pool.browse(self.cr, self.uid, user_id)
            if  result and result.partner_id:
                for job_obj in result.partner_id.job_id_history:

                    if job_obj.job_id_history.id == detail_obj.job_id.id:

                        return job_obj
        return False;
    
    def get_skp_monthly_report_raw(self,filters,skp_employee,context=None):

        period_year=skp_employee.target_period_year;
        period_month = skp_employee.target_period_month;


        user_id = False
        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]

        target_pool = self.pool.get('project.skp')
        target_ids=target_pool.search(self.cr, self.uid, [('state','not in',('draft','cancelled')),
                                                          ('target_period_year','=',period_year),
                                                          ('user_id','=',user_id),
                                                          ('target_period_month','=',period_month),
                                                          ('active', '=', True)
                                                          ]
                                      ,order='target_type_id,name', context=None)
        results = target_pool.browse(self.cr, self.uid, target_ids)
        return results;

    def get_summary_monthly_report_raw(self,filters,context=None):
        monthly_id = filters['form']['monthly_id'][0]
        skp_monthly_pool = self.pool.get('skp.employee')
        results = skp_monthly_pool.browse(self.cr, self.uid, [monthly_id,])
        if results:
            return results[0];
        return None
    def format_integer(self,val,context=None):
        try :
            return int(val)
        except:
            return None
    def format_formula_skp(self,recap_monthly):
        if not recap_monthly : return '-'
        try :
            str_jml =str(int( ( recap_monthly.jumlah_perhitungan_skp or 0) + ( recap_monthly.fn_nilai_tambahan or 0) + ( recap_monthly.fn_nilai_kreatifitas or 0) ))
            st_total_skp = str(recap_monthly.jml_skp)
            return "( %s : %s ) = "%(str_jml,st_total_skp)
        except:
            return '-'
    def format_date_month(self,val,context=None):
        try :

            val_date =  datetime.strptime(val,'%Y-%m-%d')
            if val_date.strftime('%B') != 'Pebruari' :
                fmt_val = val_date.strftime('%d %B')
            else :
                fmt_val = val_date.strftime('%d') +' ' +'Februari';

            return fmt_val
        except:
            return None

    def format_date_month_year(self, val, context=None):
        try:

            val_date = datetime.strptime(val, '%Y-%m-%d')
            if val_date.strftime('%B') != 'Pebruari':
                fmt_val = val_date.strftime('%d %B %Y')
            else:
                fmt_val = val_date.strftime('%d') + ' ' + 'Februari' + ' '+val_date.strftime('%Y');

            return fmt_val
        except:
            return None
    def format_date_id(self,val,context=None):
        try :

            val_date =  datetime.strptime(val,'%Y-%m-%d')
            if val_date.strftime('%B') != 'Pebruari' :
                fmt_val = val_date.strftime('%d %B %Y')
            else :
                fmt_val = val_date.strftime('%d') +' ' +'Februari';

            return fmt_val
        except:
            return None

    def get_target_type(self, val):
        try:
            for key in _JENIS_KEGIATAN_LIST:
                if key[0] == val :
                    return  key[1]
        except:
            return ''

        return '';

class wrapped_report_skp_target_monthly(osv.AbstractModel):
    _name = 'report.df_skp_target_monthly_report.report_skp_target_monthly_report'
    _inherit = 'report.abstract_report'
    _template = 'df_skp_target_monthly_report.report_skp_target_monthly_report'
    _wrapped_report_class = report_skp_target_monthly_report
class wrapped_report_skp_target_realisasi_monthly(osv.AbstractModel):
    _name = 'report.df_skp_target_monthly_report.report_skp_target_realisasi_monthly_report'
    _inherit = 'report.abstract_report'
    _template = 'df_skp_target_monthly_report.report_skp_target_realisasi_monthly_report'
    _wrapped_report_class = report_skp_target_monthly_report



import time
from openerp.osv import fields, osv
from openerp.tools.translate import _

class skp_target_monthly_report(osv.osv_memory):
    _name = "skp.target.monthly.report"
    
    _columns = {
        'period_year'       : fields.char('Periode Tahun', size=4, required=False),
        'period_from_date'  : fields.date('Periode Awal Pembuatan',  required=True),
        'period_to_date'    : fields.date('Periode Akhir Pembuatan',  required=True),
        'print_date'        : fields.date('Tanggal Pembuatan',  required=True),
        'user_id'           : fields.many2one('res.users', 'Pejabat Yang Dinilai'),
        'break_target'      : fields.integer('Jeda Baris TTD Target', required=True),
        'break_realisasi'   : fields.integer('Jeda Baris TTD Realisasi', required=True),
        'monthly_id'        : fields.many2one('skp.employee', 'Periode',required=False),
        'city'              : fields.char('Tempat', size=28, required=True),
        'ttd_date'          : fields.date('Tanggal Diterima',  required=True),
    } 
    _defaults = {
        'period_year':lambda *args: time.strftime('%Y'),
        #'print_date': lambda *args: time.strftime('%Y-%m-%d'),
        'user_id': lambda self, cr, uid, ctx: uid,
    }
    
    def print_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        datas = {'ids': context.get('active_ids', [])}

        res = self.read(cr, uid, ids, context=context)
        res = res and res[0] or {}
        datas.update({'form': res})
        return self.pool['report'].get_action(cr, uid, ids, 
                        'df_skp_target_monthly_report.report_skp_target_monthly_report',
                        data=datas, context=context)
    def print_realisasi_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        datas = {'ids': context.get('active_ids', [])}

        res = self.read(cr, uid, ids, context=context)
        res = res and res[0] or {}
        datas.update({'form': res})
        return self.pool['report'].get_action(cr, uid, ids,
                        'df_skp_target_monthly_report.report_skp_target_realisasi_monthly_report',
                        data=datas, context=context)
skp_target_monthly_report()
# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime

class notification_company_calculation_settings(osv.osv_memory):
    _name = "notification.company.calculation.settings"
    _columns = {
        'name': fields.char('Notif', size=128),
    }
notification_company_calculation_settings();

class res_company(osv.osv):
    _inherit = 'res.company'
    
    def action_calculate_monthly(self, cr, uid, ids, context=None):
        #res = {}
        i=0
        #user_pool = self.pool.get('res.users')
        for company_id in ids :
            try :
                print " update company by id ",company_id
                    
            except :
                i=1
        return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.company.calculation.settings',
                'target': 'new',
                'context': context,
            }

res_company()
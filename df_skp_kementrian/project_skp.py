# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from project import _KATEGORI_KEGIATAN_LIST
from project import _JENIS_KEGIATAN_LIST

_SKP_PERCENTAGE = 1.0
_PERILAKU_PERCENTAGE = 1.0
class project_skp(osv.Model):
    _inherit = 'project.skp'

    _columns = {
        'target_type_id': fields.selection(_JENIS_KEGIATAN_LIST, 'Jenis Kegiatan', required=True
        ),
        'target_category_id': fields.selection(_KATEGORI_KEGIATAN_LIST,
                                               'Kategori',
                                               readonly=True,
                                               states={'draft': [('readonly', False)], 'new': [('readonly', False)],
                                                       'correction': [('readonly', False)]},
                                               ),
        'realisasi_jumlah_kuantitas_output_file_ids': fields.many2many(
                                                'ir.attachment',
                                                'project_skp_attachment_rel',
                                                'project_skp_id',
                                                'attachment_id',
                                                'Bukti Kerja',
                                            ),

    }

    # copy from skp_employee/project_task and project multiple job
    def do_monthly_skp_summary_calculation(self, cr, uid, ids, sign, context=None):
        """ BKD->Done (Rekap Summary) """
        # Summary Calculation


        skp_employee_pool = self.pool.get('skp.employee')
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
            domain = task_obj.employee_id.id, task_obj.target_period_year, task_obj.target_period_month
            query = """ SELECT  id,coalesce(nilai_perilaku_percent,0),coalesce(nilai_skp_percent,0),coalesce(nilai_skp,0),
                            coalesce(fn_nilai_tambahan,0), coalesce(fn_nilai_kreatifitas,0)
                            FROM SKP_EMPLOYEE WHERE employee_id = %s
                            AND target_period_year = %s
                            AND target_period_month = %s
                                    """
            cr.execute(query, domain)
            rs_skp_emp = cr.fetchall()
            # print "end  search skp emp"
            if not rs_skp_emp:
                company_id = task_obj.employee_id.company_id and task_obj.employee_id.company_id.id or None
                job_id = task_obj.job_id and task_obj.job_id.id or None
                biro_id = task_obj.employee_id.biro_id and task_obj.employee_id.biro_id.id or None
                department_id = task_obj.employee_id.department_id and task_obj.employee_id.department_id.id or None
                is_kepala_opd = task_obj.employee_id.is_kepala_opd

                self.pool.get('skp.employee').insert_skp_employee_query(cr, task_obj.employee_id.id,
                                                                        task_obj.user_id.id,
                                                                        task_obj.target_period_year,
                                                                        task_obj.target_period_month, company_id,
                                                                        department_id,
                                                                        biro_id, is_kepala_opd, job_id)

                cr.execute(query, domain)
                rs_skp_emp = cr.fetchall()

            for skp_emp_id, nilai_perilaku_percent, nilai_skp_percent, nilai_skp, fn_nilai_tambahan, fn_nilai_kreatifitas in rs_skp_emp:
                nilai_perilaku_percent = 0;
                skp_state_count, jml_skp, jml_all_skp, nilai_skp, nilai_skp_percent,jumlah_perhitungan_skp = skp_employee_pool._get_detail_query_skp(
                    cr, uid, task_obj.user_id.id, task_obj.target_period_month, task_obj.target_period_year,
                    context=None)
                if nilai_skp and nilai_skp > 0:
                    nilai_skp_tambahan_percent = (nilai_skp * _SKP_PERCENTAGE) + (fn_nilai_tambahan or 0) + (
                    fn_nilai_kreatifitas or 0)
                else:
                    nilai_skp_tambahan_percent = 0.0

                nilai_total = nilai_perilaku_percent + nilai_skp_tambahan_percent
                nilai_tpp = 0

                indeks_nilai_skp = skp_employee_pool.get_indeks_nilai(cr, uid, nilai_skp, context=None)
                indeks_nilai_total = skp_employee_pool.get_indeks_nilai(cr, uid, nilai_total, context=None)

                # skp_employee_pool.write(cr , uid,skp_emp_obj['id'], update_values, context=None)
                update_query = """ UPDATE SKP_EMPLOYEE
                            SET skp_state_count = %s,
                            jml_skp = %s,
                            jml_all_skp = %s,
                            nilai_skp = %s,
                            nilai_skp_percent = %s,
                            nilai_skp_tambahan_percent = %s,
                            nilai_total = %s,
                            nilai_tpp = %s,
                            jumlah_perhitungan_skp = %s,
                            indeks_nilai_skp = %s,
                            indeks_nilai_total = %s
                            where id = %s

                                    """
                cr.execute(update_query, (skp_state_count, jml_skp, jml_all_skp,
                                          nilai_skp, nilai_skp_percent, nilai_skp_tambahan_percent,
                                          nilai_total, nilai_tpp,
                                          jumlah_perhitungan_skp,
                                          indeks_nilai_skp,
                                          indeks_nilai_total,
                                          skp_emp_id))

        return True;


project_skp()
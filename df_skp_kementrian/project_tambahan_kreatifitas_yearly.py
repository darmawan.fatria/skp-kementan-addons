# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv


class project_tambahan_kreatifitas_yearly(osv.Model):
    _inherit = 'project.tambahan.yearly'

    _columns = {
        'realisasi_rl_kepala_tugas_tambahan': fields.boolean('Kepala UPT/Atasan Langsung'),
        'suggest_rl_kepala_tugas_tambahan': fields.boolean('Kepala UPT/Atasan Langsung'),
        'appeal_rl_kepala_tugas_tambahan': fields.boolean('Kepala UPT/Atasan Langsung'),
    }


project_tambahan_kreatifitas_yearly()
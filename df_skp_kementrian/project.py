# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv

_KATEGORI_KEGIATAN_LIST = [('program', 'Program'),
                           ('kegiatan', 'Kegiatan'),
                            ('output', 'Output'),
                            ('suboutput', 'Sub Output'),
                         ]
_JENIS_KEGIATAN_LIST = [('dpa_opd_biro', 'Perjanjian Kerja'),
 ('dipa_apbn', 'Program Kerja'),
 ('sotk', 'Kegiatan Tupoksi'),
 ('lain_lain', 'Upsus'),
 ]
# to do change the value
class project_project(osv.Model):
    _inherit = 'project.project'

    _columns = {
        'target_type_id': fields.selection(_JENIS_KEGIATAN_LIST,
                                           'Jenis Kegiatan', required=True, readonly=True,
                                           states={'draft': [('readonly', False)], 'new': [('readonly', False)],
                                                   'correction': [('readonly', False)],
                                                   'confirm': [('readonly', False)]},
                                           ),
        'target_category_id': fields.selection(_KATEGORI_KEGIATAN_LIST,
                                               'Kategori',
                                               required=True,
                                               readonly=True,
                                               states={'draft': [('readonly', False)], 'new': [('readonly', False)],
                                                       'correction': [('readonly', False)]},
                                               ),
        'realisasi_jumlah_kuantitas_output_file_ids': fields.many2many(
            'ir.attachment',
            'project_project_attachment_rel',
            'project_id',
            'attachment_id',
            'Bukti Kerja',
        ), #tahunan
    }


project_project()
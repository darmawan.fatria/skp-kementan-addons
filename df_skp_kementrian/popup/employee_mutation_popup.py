##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import SUPERUSER_ID
from mx import DateTime


class employee_mutation_popup(osv.Model):
    _inherit = 'employee.mutation.popup'




    _columns = {
		'company_id': fields.many2one('res.company', 'Instansi', required=True,
									  domain="[('employee_id_kepala_instansi','!=',False)]"),
		'department_id': fields.many2one('partner.employee.department', 'Unor ess.II/UPT',required=True,
										),

	}



employee_mutation_popup()
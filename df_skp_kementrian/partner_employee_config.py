# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from project import _KATEGORI_KEGIATAN_LIST

class partner_employee_eselon(osv.Model):
    _inherit = 'partner.employee.eselon'

    _columns = {
        'target_category_id': fields.selection(_KATEGORI_KEGIATAN_LIST,
                                               'Kategori Kegiatan',
                                               ),
    }


partner_employee_eselon()
##############################################################################
#
#    Darmawan Fatriananda
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "Penerapan rule kementan",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "SKP",
    "description": """
    Penerapan kementrian RI
    """,
    "website": "http://-",
    "license": "GPL-3",
    "depends": [
        "df_project",
        "df_project_yearly",
        "df_skp_employee",
        "df_project_consolidation",
        "df_project_multiple_job"
    ],
    'data': [
           "partner_employee_config_view.xml",
           "project_skp_view.xml",
            "project_yearly_view.xml",
           "project_view.xml",
           "menu_management_view.xml",

    ],

}

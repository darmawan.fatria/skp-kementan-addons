from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import pooler
from openerp import netsvc
from openerp import workflow

class project_project_massive(osv.Model):
    _name = "project.project.massive"
    _description = "Proses Select All Selesai Target"
    def verify_all_target(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        wf_service = netsvc.LocalService("workflow")
        target_pool = pool_obj.get('project.project')
        target_ids = target_pool.read(cr, uid, context['active_ids'], ['state','employee_id','is_realisasi_ko_valid'], context=context)
        
        update_target_ids=[]
        for record in target_ids:
            if not record['employee_id'] :
                continue;
            if record['state'] not in ('evaluated'):
                continue;
            #if not record['is_realisasi_ko_valid']:
            #    continue;
            
            update_target_ids.append(record['id'])
        
        if update_target_ids:
           for target_id in update_target_ids:
               wf_service.trg_validate(uid, 'project.project', target_id, 'action_confirm', cr)
               
                
                
        return {'type': 'ir.actions.act_window_close'}
   

project_project_massive()
class project_project_evaluated_massive(osv.Model):
    _name = "project.project.evaluated.massive"
    _description = "Proses Select All Target | Atasan-BKD"
    
    def evaluated_all_target(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        wf_service = netsvc.LocalService("workflow")
        target_pool = pool_obj.get('project.project')
        target_ids = target_pool.read(cr, uid, context['active_ids'], ['state','employee_id'], context=context)
        
        update_target_ids=[]
        for record in target_ids:
            if not record['employee_id'] :
                continue;
            if record['state'] not in ('propose'):
                continue;
            update_target_ids.append(record['id'])
        
        if update_target_ids:
           for target_id in update_target_ids:
               wf_service.trg_validate(1, 'project.project', target_id, 'action_evaluated', cr)
        
                
                
        return {'type': 'ir.actions.act_window_close'}
   

project_project_evaluated_massive()

class project_project_tolak_massive(osv.Model):
    _name = "project.project.tolak.massive"
    _description = "Proses Select All Target | BKD-Pegawai"
    _columns = {
        'notes': fields.text('Catatan Petugas Verifikasi',required=True,),
        }
    def tolak_all_target(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        wf_service = netsvc.LocalService("workflow")
        target_pool = pool_obj.get('project.project')
        verifikatur_notes=''
        for o in self.browse(cr,uid,ids,context=None):
            verifikatur_notes = o.notes
        
        target_ids = target_pool.read(cr, uid, context['active_ids'], ['state','employee_id'], context=context)
        
        update_target_ids=[]
        for record in target_ids:
            if not record['employee_id'] :
                continue;
            if record['state'] not in ('evaluated'):
                continue;
            update_target_ids.append(record['id'])
        
        if update_target_ids:
           target_pool.write(cr,uid,update_target_ids, {'notes_bkd':verifikatur_notes}, context=None)
           for target_id in update_target_ids:
               wf_service.trg_validate(1, 'project.project', target_id, 'action_evaluate_rejected', cr)
        
                
                
        return {'type': 'ir.actions.act_window_close'}
   

project_project_tolak_massive()

class project_project_approve_close_massive(osv.Model):
    _name = "project.project.approve.close.massive"
    _description = "Proses Select All Target | Propose Close -Close"
    _columns = {
        'notes': fields.text('Catatan Petugas Verifikasi',required=False,),
        }
    def approve_close_all_target(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        wf_service = netsvc.LocalService("workflow")
        target_pool = pool_obj.get('project.project')
        verifikatur_notes=''
        for o in self.browse(cr,uid,ids,context=None):
            verifikatur_notes = o.notes
        
        target_ids = target_pool.read(cr, uid, context['active_ids'], ['state','employee_id'], context=context)
        
        update_target_ids=[]
        for record in target_ids:
            if not record['employee_id'] :
                continue;
            if record['state'] != 'propose_to_close':
                continue;
            update_target_ids.append(record['id'])
        
        if update_target_ids:
           target_pool.write(cr,uid,update_target_ids, {'notes_bkd':verifikatur_notes}, context=None)
           for target_id in update_target_ids:
               wf_service.trg_validate(1, 'project.project', target_id, 'action_closing_approve', cr)
        
                
                
        return {'type': 'ir.actions.act_window_close'}
   

project_project_approve_close_massive()

class project_project_approve_close_cancel_massive(osv.Model):
    _name = "project.project.approve.close.cancel.massive"
    _description = "Proses Select All Target | Propose Close -Close - But Cancel"
    _columns = {
        'notes': fields.text('Catatan Petugas Verifikasi',required=False,),
        }
    def approve_close_cancel_all_target(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        wf_service = netsvc.LocalService("workflow")
        target_pool = pool_obj.get('project.project')
        verifikatur_notes=''
        for o in self.browse(cr,uid,ids,context=None):
            verifikatur_notes = o.notes
        
        target_ids = target_pool.read(cr, uid, context['active_ids'], ['state','employee_id'], context=context)
        
        update_target_ids=[]
        for record in target_ids:
            if not record['employee_id'] :
                continue;
            if record['state'] != 'propose_to_close':
                continue;
            update_target_ids.append(record['id'])
        
        if update_target_ids:
           target_pool.write(cr,uid,update_target_ids, {'notes_bkd':verifikatur_notes}, context=None)
           for target_id in update_target_ids:
               wf_service.trg_validate(1, 'project.project', target_id, 'action_cancel_propose_closing', cr)
        
                
                
        return {'type': 'ir.actions.act_window_close'}
   

project_project_approve_close_cancel_massive()

class project_project_approve_correction_massive(osv.Model):
    _name = "project.project.approve.correction.massive"
    _description = "Proses Select All Target | Terima Pengajuan Revis"
    
    def approve_correction_all_target(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        target_pool = pool_obj.get('project.project')
        
        target_ids = target_pool.read(cr, uid, context['active_ids'], ['state','employee_id'], context=context)
        
        update_target_ids=[]
        for record in target_ids:
            if not record['employee_id'] :
                continue;
            if record['state'] != 'propose_correction':
                continue;
            update_target_ids.append(record['id'])
        
        if update_target_ids:
           for target_id in update_target_ids:
               target_pool.approve_correction(cr,uid,[target_id],context=None)
        
                
                
        return {'type': 'ir.actions.act_window_close'}
   

project_project_approve_correction_massive()


class project_project_delete_massive(osv.Model):
    _name = "project.project.delete.massive"
    _description = "Proses delete all"

    def delete_all_target(self, cr, uid, ids, context=None):

        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        target_pool = pool_obj.get('project.project')

        target_ids = target_pool.read(cr, uid, context['active_ids'], ['state', 'employee_id'], context=context)

        update_target_ids = []
        for record in target_ids:
            if not record['employee_id']:
                continue;
            if record['state'] not in ('draft','new','cancelled'):
                continue;
            update_target_ids.append(record['id'])

        if update_target_ids:
            for target_id in update_target_ids:
                target_pool.action_delete_target(cr, uid, [target_id], context=None)

        return {'type': 'ir.actions.act_window_close'}


project_project_delete_massive()

from openerp.osv import osv
from openerp.tools.translate import _
from openerp import pooler
from openerp import SUPERUSER_ID



class skp_employee_yearly_recalculate_massive_process(osv.osv):
    _name = "skp.employee.yearly.recalculate.massive.process"
    _description = "Proses Select All Calculate Tahunan"
    def skp_employee_yearly_recalculate_all(self, cr, uid, ids, context=None):

        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        skp_employee_yearly_pool = pool_obj.get('skp.employee.yearly')

        if not ids :
            return {'type': 'ir.actions.act_window_close'}
        #process_ids = []
        #for obj in skp_employee_yearly_pool.read(cr,uid,context['active_ids'],['id',], context) :
        #    process_ids.append(obj['id'])
        #for proces_is in process_ids :
        print "context['active_ids'] : "+context['active_ids']

        skp_employee_yearly_pool.do_yearly_calculation(cr, SUPERUSER_ID,context['active_ids'], context=context)

        return {'type': 'ir.actions.act_window_close'}

skp_employee_yearly_recalculate_massive_process()